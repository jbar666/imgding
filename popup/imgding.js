
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


let uiState = {};

init = () => {
    window.addEventListener("unload", function(e){
        browser.runtime.sendMessage({ command : "gui.closed"});
    }, false);

    // notify the backend to send data
    browser.runtime.sendMessage({ command : "send.data"});

    browser.runtime.onMessage.addListener((message) => {
        if (message.links) {
            // discovered links from the backend
            message.links.forEach(l => {
                updateLink(l);
            });
            updateStat();
        } else if (message.updatelink) {
            // link update
            updateLink(message.updatelink.link);
            updateStat();
        } else if (message.data) {
            // initial data after start
            initData(message.data);
            updateStat();
        } else if (message.log) {
            addLog(message.log);
        }

        if (message.defaultFolder) {
            let inputFolderName = document.getElementById("folderName");
            inputFolderName.value = message.defaultFolder;
        }

        if (message.threadTitle) {
            let threadFolder = document.getElementById("threadFolder");
            threadFolder.value = message.threadTitle;
        }
        if (message.threadId) {
            let threadId = document.getElementById("threadId");
            threadId.value = message.threadId;
        }
    });

    document.getElementById("btnDing").addEventListener("click", e => {
        let inputFolderName = document.getElementById("folderName");
        let prefixFiles =  document.getElementById("prefixFiles");
        browser.runtime.sendMessage({
                download: {
                    foldername: inputFolderName.value,
                    prefixFiles: prefixFiles.checked
                }
            }
        );
        clearLinks();
        //uiState = {...uiState, showSuccessful: true, showNew: true, showInProgress: true, showError: true};
        applyUIState();
    });

    document.getElementById("btnSuckThread").addEventListener("click", e => suckThread());

    document.getElementById("prefixFiles").addEventListener("click", e => {
        browser.storage.local.set({"prefixFiles": document.getElementById("prefixFiles").checked});
    });

    document.getElementById("btnClear").addEventListener("click", e => {
        browser.runtime.sendMessage({ command : "clear"});
        clearLinks();
        updateStat();
    });

    document.getElementById("btnRetryFailed").addEventListener("click", e => {
        browser.runtime.sendMessage({ command : "retryFailed" });
    });

    document.getElementById("tabBtnQueue").addEventListener("click", e => uiSelectTab('queue'));
    document.getElementById("tabBtnThreadSucking").addEventListener("click", e => uiSelectTab('threadSucking'));
    document.getElementById("tabBtnLog").addEventListener("click", e => uiSelectTab('log'));

    document.getElementById("chkShowNew").addEventListener("click", e => uiToggleShowSuccesful(e));
    document.getElementById("chkShowInProgress").addEventListener("click", e => uiToggleShowSuccesful(e));
    document.getElementById("chkShowSuccessful").addEventListener("click", e => uiToggleShowSuccesful(e));
    document.getElementById("chkShowError").addEventListener("click", e => uiToggleShowSuccesful(e));

    requestDefaultFolderName();

    browser.storage.local.get("prefixFiles").then(b => {
        document.getElementById("prefixFiles").checked = b.prefixFiles;
    })
};

updateUIState = (update) => {
    uiState = {...uiState, ...update};
    browser.runtime.sendMessage({uiState: uiState});
    applyUIState();
};

const applyUIState = () => {
    document.getElementById("chkShowNew").checked = uiState.showNew;
    document.getElementById("chkShowInProgress").checked = uiState.showInProgress;
    document.getElementById("chkShowSuccessful").checked = uiState.showSuccessful;
    document.getElementById("chkShowError").checked = uiState.showError;
    let linksDiv = document.getElementById("links");
    Array.from(linksDiv.childNodes).forEach(l => {
        switch (l.dataset.state){
            case "new":
                l.style.visibility = uiState.showNew ? 'visible' : 'collapse';
                break;
            case "inProgress":
                l.style.visibility = uiState.showInProgress ? 'visible' : 'collapse';
                break;
            case "ok":
                l.style.visibility = uiState.showSuccessful ? 'visible' : 'collapse';
                break;
            case "error":
                l.style.visibility = uiState.showError ? 'visible' : 'collapse';
            break;
        }
    });
};

initData = (data) => {
    data.links.forEach(l => addLink(l));
    data.log.forEach(l => addLog(l));
    uiState = data.uiState;
    applyUIState();
};

clearLinks = () => {
    let linksDiv = document.getElementById("links");
    while (linksDiv.firstChild) linksDiv.removeChild(linksDiv.firstChild);
};

addLink = (l) => {
    let linksDiv = document.getElementById("links");

    let linkDetails = document.createElement("details");
    linksDiv.appendChild(linkDetails);
    linkDetails.id = l.id;
    linkDetails.classList.add("link");
    addLinkDetails(linkDetails, l);
};

addLinkDetails = (linkDetails, l) => {
    linkDetails.dataset.state = l.state;

    let summary = document.createElement("summary");
    linkDetails.appendChild(summary);
    summary.innerText = l.link;
    summary.style.padding = "5px 5px 5px 5px";

    if (l.imageUrl) {
        let detail = createDetailEntry("Image-URL: ", l.imageUrl);
        linkDetails.appendChild(detail);
    }
    if (l.filename) {
        let detail = createDetailEntry("Filename: ", l.filename);
        linkDetails.appendChild(detail);
    }
    if (l.state === "error") {
        summary.classList.add("error");
        let detail = createDetailEntry("Error: ", l.error);
        linkDetails.appendChild(detail);
    } else if (l.state === "ok") {
        summary.classList.add("ok");
    } else if (l.state === "new"){
        summary.classList.add("new");
    } else {
        summary.classList.add("loading");
        let cancelButton = document.createElement("button");
        cancelButton.innerText = "Cancel";
        cancelButton.onclick = () => cancelDownload(l.id);
        linkDetails.appendChild(cancelButton);
    }

    switch (l.state){
        case "new":
            linkDetails.style.visibility = uiState.showNew ? 'visible' : 'collapse';
            break;
        case "inProgress":
            linkDetails.style.visibility = uiState.showInProgress ? 'visible' : 'collapse';
            break;
        case "ok":
            linkDetails.style.visibility = uiState.showSuccessful ? 'visible' : 'collapse';
            break;
        case "error":
            linkDetails.style.visibility = uiState.showError ? 'visible' : 'collapse';
            break;
    }
};

cancelDownload = (linkId) => {
    browser.runtime.sendMessage({cancelDownload: linkId});
};

clearLog = () => {
    let logDiv = document.getElementById("tabLog");
    while (logDiv.firstChild) logDiv.removeChild(logDiv.firstChild);
};

addLog = (l) => {
    let logDiv = document.getElementById("tabLog");
    let log = document.createElement("div");
    log.innerText = l;
    log.classList.add("log");
    logDiv.appendChild(log);
};

function createDetailEntry(label, text) {
    let urlDiv = document.createElement("div");
    let b = document.createElement("b");
    b.innerText = label;
    urlDiv.append(b);
    let span = document.createElement("span");
    span.innerText = text;
    urlDiv.append(span);
    return urlDiv;
}

updateLink = (l) => {
    let linkDetails = document.getElementById(l.id);
    if (linkDetails) {
        while (linkDetails.firstChild) linkDetails.removeChild(linkDetails.firstChild);
        addLinkDetails(linkDetails, l);
    } else {
        addLink(l);
    }
};

updateStat = () => {
    let total = document.getElementsByClassName("link").length;
    let ok = document.getElementsByClassName("ok").length;
    let fail = document.getElementsByClassName("error").length;
    let progress = document.getElementsByClassName("loading").length;

    let stat = document.getElementById("stat");
    while (stat.firstChild) stat.removeChild(stat.firstChild);
    let statSpan = document.createElement("span");
    statSpan.innerText = "Total: " + total + "; " + "succeeded: " + ok + "; failed: " + fail + "; in progress: " + progress;
    stat.append(statSpan);

    let widthOk = 0;
    let widthFailed = 0;
    if (total !== 0) {
        widthOk = ok / total * 100;
        widthFailed = fail / total * 100;
    }

    let progressInProgress = 100 - widthOk - widthFailed;

    document.getElementById("progressOk").style.width = "" + widthOk + "%";
    document.getElementById("progressFailed").style.width = "" + widthFailed + "%";
    document.getElementById("progressInProgress").style.width = "" + progressInProgress + "%";
};

requestDefaultFolderName = () => {
    browser.windows.getCurrent().then( w => {
            browser.tabs.query({active: true, windowId: w.id}).then(tabs => {
                for (let tab of tabs) {
                    if(tab.url.startsWith("http")) {
                        browser.tabs.executeScript(tab.id, {file: "/polyfill/browser-polyfill.min.js"})
                            .then(() => browser.tabs.executeScript(tab.id, {file: "/inject/inject-default-folder.js"}))
                            .catch(x => console.log("failed to get Tab:" + x));
                    }
                }
            });
        }
    );
};

suckThread = async () => {
    let threadFolder = document.getElementById("threadFolder").value;
    let threadId = document.getElementById("threadId").value;
    let pageFrom =  document.getElementById("pageFrom").value;
    let pageTo =  document.getElementById("pageTo").value;

    if (!threadId.match(/^\d+$/)) {
        alert("Please supply a numeric thread id.");
        return;
    }

    let w = await browser.windows.getCurrent();
    let tabs = await browser.tabs.query({active: true, windowId: w.id});
    await browser.runtime.sendMessage({
                    suckThread: {
                        threadId: threadId,
                        foldername: threadFolder,
                        pageFrom: pageFrom,
                        pageTo: pageTo,
                        url: tabs[0].url
                    }
                }
            );
    clearLinks();
};

uiSelectTab = (tab) => {
    selectTab = (id, select) => {
        document.getElementById("tabBtn" + id).classList.add(select ? 'button-selected' : 'button-normal');
        document.getElementById("tabBtn" + id).classList.remove(select ? 'button-normal': 'button-selected');
        document.getElementById("tab" + id).style.visibility = (select? 'visible' : 'collapse');
    };

    selectTab('Queue', tab === 'queue');
    selectTab('ThreadSucking', tab === 'threadSucking');
    selectTab('Log', tab === 'log');
};

const uiToggleShowSuccesful = (e) => {
    updateUIState({
        showSuccessful: document.getElementById("chkShowSuccessful").checked,
        showNew: document.getElementById("chkShowNew").checked,
        showInProgress: document.getElementById("chkShowInProgress").checked,
        showError: document.getElementById("chkShowError").checked
    });
};

init();