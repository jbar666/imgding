#!/bin/sh

rm imgding*.zip

VERSION=$(cat version)
echo Building $VERSION

zip  imgding-${VERSION}.zip -r  background/ icons/ inject/ manifest.json polyfill/ popup/ options/
