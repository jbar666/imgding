// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

isImagehost = (pageUrl, url) => {
    console.log(pageUrl)
    if (pageUrl.match('https?://(www\\.)?imagefap\\.com')){
        return url.match('imagefap.com/photo/');
    }
    return url.match('^.*hotflick\\.net\\/.*')
        || url.match('^.*imagebam\\.com\\/image.*')
        || url.match('^.*imgbox\\.com\\/.*')
        || url.match('^.*pixhost\\.to\\/.*')
        || url.match('^.*imagevenue\\.com\\/.*')
        || url.match('^.*imagetwist\\.com\\/.*')
        || url.match('^.*ibb\\.co\\/.*')
        || url.match('^.*pimpandhost\\.com\\/.*')
        || url.match('^.*turboimagehost\\.com\\/.*')
        || url.match('^.*postimg\\.(org|cc)\\/.*')
};

imgdingfunc = () => {

    let links = [];
    if (document.URL.match('^https?://xhamster\\.com/photos/gallery/.*')) {
        // Array.from does not work here 'unavailable'
        let candidates = document.querySelectorAll("a.photo-container.photo-thumb");

        for (let i = 0; i < candidates.length; i++) {
            let url = candidates.item(i).href;
            if (url.match('^https?://xhamster\\.com/photos/gallery/')) {
                links.push(url);
            }
        }
    } else {
        let candidates = Array.from(document.querySelectorAll("a img"));
        links = candidates
            .filter(c => c.parentNode.href && isImagehost(document.URL, c.parentNode.href))
            .map(c => c.parentNode.href);

    }

    browser.runtime.sendMessage(
        {
            discoveredlinks : {
                // imgdingFolderName was set via separate script as an argument
                folderName: imgdingFolderName,
                prefixFiles: imgdingPrefixFiles,
                links: links
            }
        }
    );
};

imgdingfunc();
