// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


const IMAGEHOST_PARSER = {
    handleLink: (link) => {
        link.state = STATE.IN_PROGRESS;
        let fixedUrl = IMAGEHOST_PARSER.fixImagePageUrl(link);
        fetch(fixedUrl,  {method: 'GET', credentials: 'include'})
            .then(response => response.text(), e => IMAGEHOST_PARSER.handleImagePageLoadError(link, e))
            .then(pageContent => IMAGEHOST_PARSER.handleImagePage(link, pageContent))
    },
    // some hosts (e.g. pimpanhost) have a special url for the page with the original sized image
    fixImagePageUrl: (link) => {
        let host = IMAGEHOST_PARSER.selectImageHost(link);
        if (host.fixImagePageUrl) {
            return host.fixImagePageUrl(link.link)
        } else {
            return link.link;
        }
    },
    handleImagePageLoadError: (link, e) => {
        addLog(e);
        LINKS.updateLinkState(link, STATE.ERROR, "Download of image page failed: " + link.link + ", " + e);
    },
    handleImagePage: (link, pageContent) => {
        IMAGEHOST_PARSER.extractImageInfo(link, pageContent);
        if (link.imageUrl) {
            let url = link.imageUrl;
            let filename = link.filename;
            // Firefox does not accept consecutive spaces in fie names
            filename = filename.replace(/ +/gi, ' ');
            filename = filename.replace(/[\/\\?%*:|<>]/g, "_");
            filename = "imgding/" + link.folderName + "/" + filename;
            DOWNLOADS.download(link, url, filename);
        }
    },
    extractImageInfo: (link, pageContent) => {
        let imageUrl = IMAGEHOST_PARSER.selectImageHost(link)
            .imagefunction(link.index, link.link, pageContent);
        if (!imageUrl) {
            LINKS.updateLinkState(link, STATE.ERROR, "Image URL not found on Page:" + pageContent);
            return null;
        }
        link.imageUrl = imageUrl.url;

        let filename = imageUrl.filename;
        if (link.prefixFiles) {
            let pad = "000000" + link.index;
            pad = pad.substr(pad.length-6);
            filename = pad + "_" + filename;
        }

        link.filename = filename;
        MESSENGER.sendMessageToGui(({updatelink : {link : link}}));
        return null;
    },
    selectImageHost: (link) => {
        return imagehosts.find(h => h.name === link.imagehost);
    }
};