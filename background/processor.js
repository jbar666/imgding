// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


const PROCESSOR = {
    _lastProgessIcon: null,
    _lastIconUpdateTime:0,
    _maxProcessing: 5,
    clear: () => {
        PROCESSOR._lastProgessIcon = null
    },
    processQueue: () => {
        let countProcessing = LINKS.getCountProcessing();

        LINKS.getNewLinks(PROCESSOR._maxProcessing - countProcessing)
            .forEach( l => IMAGEHOST_PARSER.handleLink(l));

        let countProcessingOrNew = LINKS.getCountProcessingOrNew();
        if (countProcessingOrNew === 0){
            let retryies = LINKS.filter(l => l.state === STATE.ERROR && l.imagehost && !l.retry);
            if (retryies.length > 0) {
                retryies.forEach( l => {
                    l.retry = true;
                    l.state = STATE.NEW;
                    MESSENGER.sendMessageToGui({updatelink : {link : l}});
                });
                PROCESSOR.processQueue();
            }
        }
        PROCESSOR.updateIcon();
    },
    retryFailed: () => {
        LINKS.filter(l => l.error).forEach(l => {
            let p = null;
            if (l.downloadId) {
                p = browser.downloads.removeFile(l.downloadId);
            } else {
                p = Promise.resolve();
            }
            p.catch((x) => addLog("removeFile failed: " + x))
                .then(() => LINKS.updateLinkState(l, STATE.NEW, null));
        });
    },
    updateIcon: () => {

        let countProcessingOrNew = LINKS.filter(l => l.state === STATE.IN_PROGRESS || l.state === STATE.NEW).length;
        if (countProcessingOrNew === 0){
            if (PROCESSOR._lastProgessIcon !== null) {
                browser.browserAction.setIcon({path: "icons/imgding-48.png"});
                PROCESSOR._lastProgessIcon = null;
            }
        } else {
            if (Date.now() - PROCESSOR._lastIconUpdateTime > 5000) {
                PROCESSOR._lastIconUpdateTime = Date.now();
                let counts = LINKS.getCounts();
                let okHeight = 48 * (counts.ok/counts.total);
                let errorHeight = Math.ceil(48 * (counts.error/counts.total));

                if (PROCESSOR._lastProgessIcon === null || PROCESSOR._lastProgessIcon.okHeight !== okHeight || PROCESSOR._lastProgessIcon.errorHeight !== errorHeight) {
                    let canvas = document.createElement("canvas");
                    let ctx = canvas.getContext("2d");
                    ctx.fillStyle = "white";
                    ctx.fillRect(0, 0, 48, 48);
                    ctx.fillStyle = "green";
                    ctx.fillRect(0, 48-okHeight, 48, okHeight);
                    ctx.fillStyle = "red";
                    ctx.fillRect(0, 48-okHeight-errorHeight, 48, errorHeight);
                    let imageData = ctx.getImageData(0, 0, 48, 48);
                    browser.browserAction.setIcon({imageData: imageData});
                    PROCESSOR._lastProgessIcon = {okHeight: okHeight, errorHeight: errorHeight};
                }
            }
        }
    }
};