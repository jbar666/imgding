function saveOptions(e) {
    browser.storage.local.set({
        maxparallel: document.querySelector("#maxparallel").value,
        timeout: document.querySelector("#timeout").value
    }).then(() => browser.runtime.sendMessage({ command: "reloadPreferences"}));
    e.preventDefault();
    window.close();
}

function restoreOptions() {
    let storageItem = browser.storage.local.get('maxparallel');
    storageItem.then((res) => {
        document.querySelector("#maxparallel").value = res.maxparallel || 5;
    },() => {
        document.querySelector("#maxparallel").value = 5;
    });
    storageItem = browser.storage.local.get('timeout');
    storageItem.then((res) => {
        document.querySelector("#timeout").value = res.timeout || 0;
    },() => {
        document.querySelector("#timeout").value = 0;
    });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);